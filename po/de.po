# German translation for gnome-2048.
# Copyright (C) 2015 gnome-2048's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-2048 package.
# Christian Kirbach <Christian.Kirbach@gmail.com>, 2015.
# Bernd Homuth <dev@hmt.im>, 2015, 2016.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2016, 2018.
# rugk <rugk+i18n@posteo.de>, 2019.
# Tim Sabsch <tim@sabsch.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-2048 master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-2048/issues\n"
"POT-Creation-Date: 2020-07-05 20:35+0000\n"
"PO-Revision-Date: 2020-09-02 12:40+0200\n"
"Last-Translator: Christian Kirbach <christian.kirbach@gmail.com>\n"
"Language-Team: Deutsch <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#. Translators: title of the dialog that appears (with default settings) when you reach 2048
#: data/congrats.ui:23
msgid "Congratulations!"
msgstr "Glückwunsch!"

#. Translators: button in the "Congratulations" dialog that appears (with default settings) when you reach 2048 (with a mnemonic that appears pressing Alt)
#. Translators: button in the headerbar (with a mnemonic that appears pressing Alt)
#: data/congrats.ui:46 data/game-headerbar.ui:29
msgid "_New Game"
msgstr "_Neues Spiel"

#. Translators: button in the "Congratulations" dialog that appears (with default settings) when you reach 2048; the player can continue playing after reaching 2048 (with a mnemonic that appears pressing Alt)
#: data/congrats.ui:54
msgid "_Keep Playing"
msgstr "_Weiterspielen"

#. Translators: title of the window, displayed in the headerbar
#: data/game-headerbar.ui:24 data/org.gnome.TwentyFortyEight.appdata.xml.in:8
msgid "GNOME 2048"
msgstr "GNOME 2048"

#. Translators: header of the "Play with arrows" shortcut section
#: data/help-overlay.ui:29
msgctxt "shortcut window"
msgid "Game"
msgstr "Spiel"

#. Translators: the application has no help, so the use of arrows for playing is documented here
#: data/help-overlay.ui:34
msgctxt "shortcut window"
msgid "Play with arrows"
msgstr "Mit den Pfeiltasten spielen"

#. Translators: header of the "Choose a new game" and "Start a new game" shortcuts section
#: data/help-overlay.ui:44
msgctxt "shortcut window"
msgid "New game"
msgstr "Neues Spiel"

#. Translators: shortcut that opens the New Game menu, allowing to choose the board size of the next game
#: data/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Choose a new game"
msgstr "Ein neues Spiel wählen"

#. Translators: shortcut that starts immediately a new game, with similar options
#: data/help-overlay.ui:57
msgctxt "shortcut window"
msgid "Start a new game"
msgstr "Ein neues Spiel starten"

#. Translators: header of the "Toggle main menu," "Keyboard shortcuts," "About," and "Quit," shortcuts section
#: data/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Generic"
msgstr "Allgemein"

#. Translators: shortcut that toggles the hamburger menu
#: data/help-overlay.ui:72
msgctxt "shortcut window"
msgid "Toggle main menu"
msgstr "Hauptmenü umschalten"

#. Translators: shortcut that opens Keyboard Shortcuts window
#: data/help-overlay.ui:80
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Tastenkombinationen"

#. Translators: shortcut that opens About dialog
#: data/help-overlay.ui:88
msgctxt "shortcut window"
msgid "About"
msgstr "Info"

#. Translators: shortcut that quits application
#: data/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Quit"
msgstr "Beenden"

#: data/org.gnome.TwentyFortyEight.appdata.xml.in:9
#: data/org.gnome.TwentyFortyEight.desktop.in:4
msgid "Obtain the 2048 tile"
msgstr "Erlangen Sie die 2048-Kachel"

# Schreibweise mit zwei Wörtern für "süchtig machend" nach https://de.wiktionary.org/wiki/addictif
#: data/org.gnome.TwentyFortyEight.appdata.xml.in:11
msgid ""
"Play the highly addictive 2048 game. GNOME 2048 is a clone of the popular "
"single-player puzzle game. Gameplay consists of joining numbers in a grid "
"and obtain the 2048 tile."
msgstr ""
"Spielen Sie das süchtig machende Spiel »2048«. GNOME 2048 ist ein Klon des "
"beliebten Einzelspieler-Puzzles. Ziel des Spiels ist das Zusammenfügen von "
"Zahlen in einem Raster, um die 2048-Kachel zu erlangen."

#: data/org.gnome.TwentyFortyEight.appdata.xml.in:16
msgid ""
"Use your keyboard's arrow keys to slide all tiles in the desired direction. "
"Be careful: all tiles slide to their farthest possible positions, you cannot "
"slide just one tile or one row or column. Tiles with the same value are "
"joined when slided one over the other."
msgstr ""
"Verwenden Sie die Pfeiltasten Ihres Keyboards, um alle auf dem Spielfeld "
"befindlichen Kacheln in die gewünschte Richtung zu bewegen. Aber Vorsicht, "
"alle Kacheln werden so weit wie möglich rutschen. Es ist nicht möglich "
"einzelne Kacheln, Spalten oder Zeilen zu verschieben. Kacheln, die den "
"gleichen Wert haben, werden beim Aufeinandertreffen zusammengefügt und "
"erhalten den addierten Wert."

#: data/org.gnome.TwentyFortyEight.appdata.xml.in:22
msgid ""
"With every new tile obtained you increase your score. If you think you can "
"easily get the 2048 tile, do not let it stop you, the game does not end "
"there, you can continue joining tiles and improving your score."
msgstr ""
"Mit jeder neuen Kachel, die auf dem Spielfeld erscheint, erhöhen Sie Ihren "
"Punktestand. Wenn Sie ohne Probleme die 2048 erreichen, können Sie natürlich "
"weiterspielen und weitere Punkte sammeln, um einen noch höheren Punktestand "
"zu erreichen."

#: data/org.gnome.TwentyFortyEight.appdata.xml.in:27
msgid ""
"Originally created by Gabriele Cirulli, 2048 has gained much popularity due "
"to it being highly addictive. Cirulli's 2048 is in turn a clone of the 1024 "
"game and includes ideas from other clones."
msgstr ""
"Erfunden von Gabriele Cirulli ist 2048 ist ein allseits beliebtes Spiel, "
"dessen Erfolg auf seinem Suchtfaktor beruht. Cirullis 2048 ist wiederum ein "
"Klon des 1024-Spiels und beinhaltet zusätzlich Ideen anderer Klone."

#: data/org.gnome.TwentyFortyEight.appdata.xml.in:36
msgid "A running game having reached the 2048 tile"
msgstr "Ein Spiel, das gerade die 2048-Kachel erreicht hat"

#. this is a translatable version of project_group
#: data/org.gnome.TwentyFortyEight.appdata.xml.in:50
msgid "The GNOME Project"
msgstr "Das GNOME-Projekt"

#. Translators: about dialog text; the program name
#: data/org.gnome.TwentyFortyEight.desktop.in:3 src/game-window.vala:495
msgid "2048"
msgstr "2048"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.TwentyFortyEight.desktop.in:14
msgid "puzzle;"
msgstr "Puzzle;"

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/window-width'
#: data/org.gnome.TwentyFortyEight.gschema.xml:6
msgid "Window width"
msgstr "Fensterbreite"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/window-width'
#: data/org.gnome.TwentyFortyEight.gschema.xml:8
msgid "Window width."
msgstr "Fensterbreite."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/window-height'
#: data/org.gnome.TwentyFortyEight.gschema.xml:13
msgid "Window height"
msgstr "Fensterhöhe"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/window-height'
#: data/org.gnome.TwentyFortyEight.gschema.xml:15
msgid "Window height."
msgstr "Fensterhöhe."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/window-maximized'
#: data/org.gnome.TwentyFortyEight.gschema.xml:20
msgid "Window maximized"
msgstr "Fenster maximiert"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/window-maximized'
#: data/org.gnome.TwentyFortyEight.gschema.xml:22
msgid "Window maximization state."
msgstr "Maximierungszustand des Fensters."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/rows'
#: data/org.gnome.TwentyFortyEight.gschema.xml:28
msgid "Number of rows"
msgstr "Zeilenanzahl"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/rows'
#: data/org.gnome.TwentyFortyEight.gschema.xml:30
msgid "Game grid number of rows."
msgstr "Anzahl der Zeilen im Spielfeldraster."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/cols'
#: data/org.gnome.TwentyFortyEight.gschema.xml:36
msgid "Number of columns"
msgstr "Spaltenanzahl"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/cols'
#: data/org.gnome.TwentyFortyEight.gschema.xml:38
msgid "Game grid number of columns."
msgstr "Anzahl der Spalten im Spielfeldraster."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/target-value'
#: data/org.gnome.TwentyFortyEight.gschema.xml:44
msgid "Target value"
msgstr "Zielwert"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/target-value'
#: data/org.gnome.TwentyFortyEight.gschema.xml:46
msgid "Tile value at which user is congratulated."
msgstr "Kachelwert, ab dem der Nutzer beglückwünscht wird."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/do-congrat'
#: data/org.gnome.TwentyFortyEight.gschema.xml:51
msgid "Congrat on target tile"
msgstr "Glückwünsche zur Ziel-Kachel"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/do-congrat'
#: data/org.gnome.TwentyFortyEight.gschema.xml:53
msgid "Whether the user shall be congratulated on obtaining target tile."
msgstr ""
"Legt fest, ob der Nutzer bei Erreichen der Ziel-Kachel beglückwünscht wird."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/animations-speed'
#: data/org.gnome.TwentyFortyEight.gschema.xml:60
msgid "Animations speed"
msgstr "Animationsgeschwindigkeit"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/animations-speed'
#: data/org.gnome.TwentyFortyEight.gschema.xml:62
msgid "Duration of animations: show tile, move tile, and dim tile."
msgstr ""
"Dauer der Animation: Titel zeigen, Kachel bewegen und Kachel verdunkeln."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/allow-undo'
#: data/org.gnome.TwentyFortyEight.gschema.xml:67
msgid "Allow undo"
msgstr "Zurücknehmen erlauben"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/allow-undo'
#: data/org.gnome.TwentyFortyEight.gschema.xml:69
msgid "Whether tile movements can be undone."
msgstr "Legt fest, ob Kachelzüge rückgängig gemacht werden können."

#. Translators: summary of a settings key, see 'dconf-editor /org/gnome/2048/allow-undo-max'
#: data/org.gnome.TwentyFortyEight.gschema.xml:75
msgid "Number of undo movements"
msgstr "Anzahl Züge zum Zurücknehmen"

#. Translators: description of a settings key, see 'dconf-editor /org/gnome/2048/allow-undo-max'
#: data/org.gnome.TwentyFortyEight.gschema.xml:77
msgid "Maximum number of tile movements that can be undone."
msgstr "Maximale Anzahl von Zügen, die rückgängig gemacht werden können."

#. Translators: command-line option description, see 'gnome-2048 --help'
#: src/application.vala:36
msgid "Play in the terminal (see “--cli=help”)"
msgstr "Im Terminal spielen (siehe »--cli=help«)"

#. Translators: in the command-line options description, text to indicate the user should give a command after '--cli' for playing in the terminal, see 'gnome-2048 --help'
#: src/application.vala:39
msgid "COMMAND"
msgstr "BEFEHL"

#. Translators: command-line option description, see 'gnome-2048 --help'
#: src/application.vala:42
msgid "Start new game of given size"
msgstr "Ein neues Spiel in gewünschter Größe starten"

#. Translators: in the command-line options description, text to indicate the user should specify a size after '--size', see 'gnome-2048 --help'
#: src/application.vala:45
msgid "SIZE"
msgstr "GRÖßE"

#. Translators: command-line option description, see 'gnome-2048 --help'
#: src/application.vala:48
msgid "Print release version and exit"
msgstr "Veröffentlichungsversion ausgeben und verlassen"

#. Translators: command-line error message, displayed for an incorrect game size request; try 'gnome-2048 -s 0'
#: src/application.vala:122
msgid "Failed to parse size. Size must be between 2 and 9, or in the form 2x3."
msgstr ""
"Fehler beim Verarbeiten der Größenangabe. Die Größe muss zwischen 2 und 9 "
"oder in der Form 2x3 vorliegen."

#. Translators: subtitle of the headerbar, when the user cannot move anymore
#: src/game-headerbar.vala:67
msgid "Game Over"
msgstr "Spiel vorbei"

#. Translators: entry in the hamburger menu, if the "Allow undo" option is set to true
#: src/game-headerbar.vala:97
msgid "Undo"
msgstr "Rückgängig machen"

#. Translators: entry in the hamburger menu; opens a window showing best scores
#: src/game-headerbar.vala:108
msgid "Scores"
msgstr "Punkteliste"

#. Translators: usual menu entry of the hamburger menu
#: src/game-headerbar.vala:119
msgid "Keyboard Shortcuts"
msgstr "Tastenkombinationen"

#. Translators: entry in the hamburger menu
#: src/game-headerbar.vala:122
msgid "About 2048"
msgstr "Info zu 2048"

#. Translators: on main window, entry of the menu when clicking on the "New Game" button; to change grid size to 3 × 3
#: src/game-headerbar.vala:142
msgid "3 × 3"
msgstr "3 × 3"

#. Translators: on main window, entry of the menu when clicking on the "New Game" button; to change grid size to 4 × 4
#: src/game-headerbar.vala:148
msgid "4 × 4"
msgstr "4 × 4"

#. Translators: on main window, entry of the menu when clicking on the "New Game" button; to change grid size to 5 × 5
#: src/game-headerbar.vala:154
msgid "5 × 5"
msgstr "5 × 5"

#. Translators: command-line warning displayed if the user manually sets a invalid grid size
#: src/game-headerbar.vala:163
msgid "Grids of size 1 by 2 are disallowed."
msgstr "Raster der Größe 1 oder 2 sind nicht erlaubt."

#. Translators: on main window, entry of the menu when clicking on the "New Game" button; appears only if the user has set rows and cols manually
#: src/game-headerbar.vala:167
msgid "Custom"
msgstr "Benutzerdefiniert"

#. Translators: text of the dialog that appears when the user obtains the first 2048 tile in the game; the %u is replaced by the number the user wanted to reach (usually, 2048)
#: src/game-window.vala:415
#, c-format
msgid "You have obtained the %u tile for the first time!"
msgstr "Sie haben die %u-Kachel zum ersten Mal erreicht!"

#. Translators: combobox entry in the dialog that appears when the user clicks the "Scores" entry in the hamburger menu, if the user has already finished at least one 3 × 3 game and one of other size
#: src/game-window.vala:434
msgid "Grid 3 × 3"
msgstr "Raster 3 × 3"

#. Translators: combobox entry in the dialog that appears when the user clicks the "Scores" entry in the hamburger menu, if the user has already finished at least one 4 × 4 game and one of other size
#: src/game-window.vala:437
msgid "Grid 4 × 4"
msgstr "Raster 4 × 4"

#. Translators: combobox entry in the dialog that appears when the user clicks the "Scores" entry in the hamburger menu, if the user has already finished at least one 5 × 5 game and one of other size
#: src/game-window.vala:440
msgid "Grid 5 × 5"
msgstr "Raster 5 × 5"

#. Translators: label introducing a combobox in the dialog that appears when the user clicks the "Scores" entry in the hamburger menu, if the user has already finished at least two games of different size (between 3 × 3, 4 × 4 and 5 × 5)
#: src/game-window.vala:443
msgid "Grid Size:"
msgstr "Rastergröße:"

#. Translators: about dialog text; a introduction to the game
#: src/game-window.vala:499
msgid "A clone of 2048 for GNOME"
msgstr "Ein Klon von 2048 für GNOME"

#. Translators: text crediting a maintainer, seen in the About dialog
#: src/game-window.vala:504
msgid "Copyright © 2014-2015 – Juan R. García Blanco"
msgstr "Copyright © 2014-2015 – Juan R. García Blanco"

#. Translators: text crediting a maintainer, seen in the About dialog; the %u are replaced with the years of start and end
#: src/game-window.vala:508
#, c-format
msgid "Copyright © %u-%u – Arnaud Bonatti"
msgstr "Copyright © %u-%u – Arnaud Bonatti"

#. Translators: about dialog text; this string should be replaced by a text crediting yourselves and your translation team, or should be left empty. Do not translate literally!
#: src/game-window.vala:513
msgid "translator-credits"
msgstr ""
"Christian Kirbach <Christian.Kirbach@gmail.com>\n"
"Bernd Homuth <dev@hmt.im>\n"
"Mario Blättermann <mario.blaettermann@gmail.com>\n"
"rugk <rugk+i18n@posteo.de>\n"
"Tim Sabsch <tim@sabsch.com>\n"
"Stephan Woidowski <swoidowski@t-online.de>"

#. Translators: about dialog text; label of the website link
#: src/game-window.vala:517
msgid "Page on GNOME wiki"
msgstr "Seite im GNOME-Wiki"

#~ msgid "org.gnome.TwentyFortyEight"
#~ msgstr "org.gnome.TwentyFortyEight"

#~ msgid "Preferences"
#~ msgstr "Einstellungen"

#~ msgid "gnome-2048"
#~ msgstr "gnome-2048"

#~ msgid "Grid size"
#~ msgstr "Rastergröße"

#~ msgid "Display congrats"
#~ msgstr "Glückwünsche anzeigen"
